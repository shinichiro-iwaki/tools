# tools

[豆蔵デベロッパーサイト](https://developer.mamezou-tech.com/)のブログ記事で利用しているサンプルコード  

## サンプルについて

### 概要  
PowequeryをつかってGitlab.comからデータを取得する簡単なサンプルです。  
gitlabのホスト、データ取得先のユーザーID、GitlabのアクセストークンはPowerQueryの変数で設定できます。  
初期状態ではGitlabアクセストークンにダミー値が設定してあるので、read_api以上の権限を持つトークンを発行して設定してください。  

## 参照記事

以下の記事で参照しています。

| 記事 | branch | tags | 
| ---- | ---- | ---- | 
| [Excelを使って簡単にオープンデータを分析する(発展編)](https://developer.mamezou-tech.com/blogs/2024/05/20/powerquery-introduction/) | main | ― | 
  
